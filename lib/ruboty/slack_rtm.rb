require 'ruboty/slack_rtm/version'

require 'ruboty/slack_rtm/user'
require 'ruboty/slack_rtm/channel'
require 'ruboty/slack_rtm/message'
require 'ruboty/slack_rtm/client'

require 'ruboty/adapters/slack_rtm'
