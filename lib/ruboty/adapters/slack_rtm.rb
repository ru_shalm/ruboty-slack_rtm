require 'ruboty'

module Ruboty
  module Adapters
    class SlackRTM < Base
      env :SLACK_TOKEN, 'Slack API Token'

      def run
        connect
      end

      def say(message)
        client.say(
          message[:original][:channel_id],
          message[:body]
        )
      end

      private

      def client
        @client ||= Ruboty::SlackRTM::Client.new(token)
      end

      def token
        ENV['SLACK_TOKEN'] || ''
      end

      def connect
        client.on_message(method(:on_message))
        client.init
        ENV['RUBOTY_NAME'] ||= client.myself.name
        client.connect
      end

      def on_message(message)
        robot.receive(
          body: message.text,
          from: message.user.name,
          from_name: message.user.name,
          channel_id: message.channel.id,
          channel_name: message.channel.name,
        )
      end
    end
  end
end
