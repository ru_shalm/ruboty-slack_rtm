# encoding: utf-8
require 'virtus'

module Ruboty
  module SlackRTM
    class Channel
      include Virtus.model

      attribute :id, String
      attribute :name, String

      def self.load(data)
        self.new.tap do |channel|
          channel.id = data['id']
          channel.name = data['name']
        end
      end
    end
  end
end
