# encoding: utf-8
require 'virtus'

module Ruboty
  module SlackRTM
    class Message
      include Virtus.model

      attribute :user, User
      attribute :channel, Channel
      attribute :text, String
    end
  end
end
