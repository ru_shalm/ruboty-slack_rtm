# encoding: utf-8
require 'virtus'

module Ruboty
  module SlackRTM
    class User
      include Virtus.model

      attribute :id, String
      attribute :name, String
      attribute :is_bot, Boolean

      def bot?
        !!@is_bot
      end

      def self.load(data)
        self.new.tap do |user|
          user.id = data['id']
          user.name = data['name']
          user.is_bot = data['is_bot']
        end
      end
    end
  end
end
