# encoding: utf-8

require 'json'
require 'rest-client'
require 'eventmachine'
require 'faye/websocket'

module Ruboty
  module SlackRTM
    class Client
      START_URI = 'https://slack.com/api/rtm.start'
      USER_INFO_URI = 'https://slack.com/api/channels.info'
      CHANNEL_INFO_URI = 'https://slack.com/api/channels.info'

      def initialize(token)
        @token = token
        @myself = nil
        @users = {}
        @channels = {}
      end
      attr_reader :myself
      attr_reader :users
      attr_reader :channels

      def on_message(proc)
        @on_message = proc
      end

      def init
        raise 'undefined on_message' unless @on_message

        data = JSON.parse(RestClient.get("#{START_URI}?token=#{@token}"))
        raise 'cannot connect' unless data['ok']

        data['users'].each do |user_data|
          user = User.load(user_data)
          @users[user.id] = user
        end
        @myself = @users[data['self']['id']]

        data['channels'].each do |channel_data|
          channel = Channel.load(channel_data)
          @channels[channel.id] = channel
        end

        @url = data['url']
      end

      def connect
        raise 'not initialized' unless @url
        EM.run do
          begin
            @ws = Faye::WebSocket::Client.new(@url)
            @ws.on :message do |event|
              begin
                message = JSON.parse(event.data)
                puts message
                case message['type']
                when 'channel_joined', 'channel_rename', 'group_joined', 'group_rename'
                  channel = Channel.load(message['channel'])
                  @channels[channel.id] = channel
                when 'im_open'
                  channel = Channel.new(id: message['channel'], name: fetch_user(message['user']).name)
                  @channels[channel.id] = channel
                when 'message'
                  from_user = fetch_user(message['user'])
                  channel = fetch_channel(message['channel'])
                  message['text'].gsub!(/\<@([A-Za-z0-9]+)\>/) do
                    "@#{fetch_user($1).name}"
                  end
                  message['text'].gsub!(/\<#([A-Za-z0-9]+)\>/) do
                    "##{fetch_channel($1).name}"
                  end
                  message = Message.new(
                    user: from_user,
                    channel:channel,
                    text: message['text'],
                  )
                  @on_message.call(message)
                end
              rescue => e
                puts e
                puts e.backtrace
              end
            end
          rescue => e
            @ws = nil
            puts e.inspect
            sleep 5
            retry
          end
        end
      end

      def say(channel, text)
        raise 'not connection' unless @ws
        @ws.send({
          id: 1,
          type: 'message',
          channel: channel.kind_of?(String) ? channel : channel.id,
          text: text,
        }.to_json)
      end

      def fetch_user(user_id)
        unless @users.has_key?(user_id)
          data = JSON.parse(RestClient.get("#{USER_INFO_URI}?token=#{@token}&user=#{user_id}"))
          user = User.load(data['user'])
          @users[user.id] = user
        end
        @users[user_id]
      end

      def fetch_channel(channel_id)
        unless @channels.has_key?(channel_id)
          channel = Channel.new(id: channel_id)
          case channel_id[0]
          when 'D' # TODO: DM user name
          when 'G' # TODO: Group name
          else
            data = JSON.parse(RestClient.get("#{CHANNEL_INFO_URI}?token=#{@token}&channel=#{channel_id}"))
            channel = Channel.load(data['channel'])
          end
          @channels[channel.id] = channel
        end
        @channels[channel_id]
      end
    end
  end
end
