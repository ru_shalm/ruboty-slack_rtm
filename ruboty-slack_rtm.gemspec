# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ruboty/slack_rtm/version'

Gem::Specification.new do |spec|
  spec.name          = "ruboty-slack_rtm"
  spec.version       = Ruboty::SlackRTM::VERSION
  spec.authors       = ["Ru/MuckRu"]
  spec.email         = ["ru_shalm@hazimu.com"]
  spec.summary       = 'Slack Real Time Messaging(RTM) API adapter for Ruboty'
  spec.description   = 'Slack Real Time Messaging(RTM) API adapter for Ruboty'
  spec.homepage      = "http://github.com/rutan/ruboty-slack_rtm"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'ruboty', '>= 1.1.2'
  spec.add_dependency 'rest-client', '>= 1.7.2'
  spec.add_dependency 'faye-websocket', '>= 0.9.0'
  spec.add_dependency 'virtus', '>= 1.0.3'

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "pry"
end
